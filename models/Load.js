const { Schema, model, Types } = require("mongoose")

const schema = new Schema(
    {
        created_by: { type: String, required: true },
        assigned_to: { type: String, default: "" },
        status: {
            type: String,
            default: "NEW",
            enum: ["NEW", "POSTED", "ASSIGNED", "SHIPPED"],
        },
        state: {
            type: String,
            default: "",
            enum: [
                "",
                "En route to Pick Up",
                "Arrived to Pick Up",
                "En route to delivery",
                "Arrived to delivery",
            ],
        },
        name: { type: String, required: true },
        payload: { type: Number, required: true },
        pickup_address: { type: String, required: true },
        delivery_address: { type: String, required: true },
        dimensions: {
            width: {
                require: true,
                type: Number,
            },
            length: {
                require: true,
                type: Number,
            },
            height: {
                require: true,
                type: Number,
            },
        },
        logs: [{ message: String, time: { type: Date, default: Date.now } }],
        created_date: {
            type: Date,
            default: Date.now,
        },
    },
    { versionKey: false }
)

module.exports = model("Load", schema)
