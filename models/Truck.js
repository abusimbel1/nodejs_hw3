const { Schema, model, Types } = require("mongoose")

const schema = new Schema(
    {
        created_by: { type: String, required: true },
        assigned_to: { type: String, default: "" },
        type: {
            type: String,
            required: true,
            enum: ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"],
        },
        status: {
            type: String,
            default: "",
            enum: ["OL", "IS", ""],
        },
        created_date: {
            type: Date,
            default: Date.now,
        },
    },
    { versionKey: false }
)

module.exports = model("Truck", schema)
