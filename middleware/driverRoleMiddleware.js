module.exports = (req, res, next) => {
    if (req.user.role !== "DRIVER") {
        return res.status(400).json({ message: "Missing user role" })
    } else {
        next()
    }
}
