const Truck = require("../models/Truck")

module.exports = async (req, res, next) => {
    const truck = await Truck.findOne({
        assigned_to: req.user._id,
        status: "OL",
    })
    if (truck) {
        return res.status(400).json({ message: "Driver is in load" })
    } else {
        next()
    }
}
