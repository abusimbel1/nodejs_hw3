const { Router } = require("express")
const router = Router()
const auth = require("../middleware/authMiddleware")
const isDriver = require("../middleware/driverRoleMiddleware")
const isShipper = require("../middleware/shipperRoleMiddleware")
const { sendEmail } = require("../additionalFunc/additionalFunc")
const { check, validationResult, checkSchema } = require("express-validator")
// const { isDriverFree } = require("../additionalFunc/additionalFunc")
const driverFree = require("../middleware/driverFree")
const User = require("../models/User")
const Truck = require("../models/Truck")
const Load = require("../models/Load")

var schema = {
    type: {
        in: "body",
        isIn: {
            options: [["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"]],
            errorMessage: "Invalid type",
        },
    },
}

//api/trucks/
router.get("/", auth, isDriver, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        const trucks = await Truck.find({ created_by: user._id })

        res.status(200).json([{ trucks }])
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

//api/trucks/:id
router.get("/:id", auth, isDriver, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        const { id } = req.params
        if (!id) {
            return res.status(400).json({ message: "Truck not found" })
        }

        const truck = await Truck.findById(id)

        if (truck && truck.created_by == user._id) {
            return res.status(200).json({ truck })
        } else {
            return res.status(400).json({
                message: "Truck not found",
            })
        }
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

// api/trucks/
router.post(
    "/",
    [checkSchema(schema)],
    auth,
    isDriver,
    driverFree,
    async (req, res) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: "Incorrect truck data",
                })
            }
            const user = await User.findById(req.user._id)
            if (!user) {
                return res.status(400).json({ message: "User not found" })
            }

            const { type } = req.body

            const truck = new Truck({
                created_by: user._id,
                type,
            })
            sendEmail(user.email, "Truck created successfully", "Truck created")
            await truck.save()

            res.status(201).json({ message: "Truck created successfully" })
        } catch (e) {
            res.status(500).json({ message: "Something went wrong, try again" })
        }
    }
)

// api/trucks/
router.post("/:id/assign", auth, isDriver, driverFree, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }

        const { id } = req.params
        if (!id) {
            return res.status(400).json({ message: "Truck not found" })
        }
        const truck = await Truck.findById(id)

        const isDriverInLoad = await Load.find({
            assigned_to: user._id,
            status: "ASSIGNED",
        })
        if (isDriverInLoad.length > 0) {
            return res.status(400).json({ message: "Driver is in load" })
        }
        if (truck && truck.assigned_to == "" && truck.created_by == user._id) {
            await Truck.findOneAndUpdate(
                { assigned_to: user._id },
                { assigned_to: "", status: "" }
            )
            await Truck.findByIdAndUpdate(id, {
                assigned_to: user._id,
                status: "IS",
            })

            res.status(200).json({ message: "Truck assigned successfully" })
        } else {
            res.status(400).json({
                message: "Truck not found",
            })
        }
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})
// api/trucks/:id
router.put(
    "/:id",
    [checkSchema(schema)],
    auth,
    isDriver,
    driverFree,
    async (req, res) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: "Incorrect truck data",
                })
            }
            const user = await User.findById(req.user._id)
            if (!user) {
                return res.status(400).json({ message: "User not found" })
            }

            const { id } = req.params
            if (!id) {
                return res
                    .status(400)
                    .json({ message: "Truck is not a driver" })
            }
            const { type } = req.body

            const truck = await Truck.findById(id)

            if (truck && truck.created_by == user._id && truck.status != "OL") {
                await Truck.findByIdAndUpdate(id, { type })
                sendEmail(
                    user.email,
                    "Truck details changed",
                    "Truck details changed successfully"
                )
                res.status(200).json({
                    message: "Truck details changed successfully",
                })
            } else {
                res.status(400).json({
                    message: "Truck not found",
                })
            }
        } catch (e) {
            res.status(500).json({ message: "Something went wrong, try again" })
        }
    }
)
// api/trucks/:id
router.delete("/:id", auth, isDriver, driverFree, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        const { id } = req.params
        if (!id) {
            return res.status(400).json({ message: "Truck not found" })
        }
        const truck = await Truck.findById(id)

        if (truck && truck.created_by == user._id && truck.status != "OL") {
            await Truck.findByIdAndDelete(id)
            return res.status(200).json({
                message: "Truck deleted successfully",
            })
        } else {
            return res.status(400).json({
                message: "Truck not found",
            })
        }
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

module.exports = router
