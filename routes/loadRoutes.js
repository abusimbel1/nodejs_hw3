const { Router } = require("express")
const router = Router()
const auth = require("../middleware/authMiddleware")
const isDriver = require("../middleware/driverRoleMiddleware")
const isShipper = require("../middleware/shipperRoleMiddleware")
const { check, validationResult, checkSchema } = require("express-validator")
const { findTruck, sendEmail } = require("../additionalFunc/additionalFunc")
const User = require("../models/User")
const Truck = require("../models/Truck")
const Load = require("../models/Load")
const { findByIdAndUpdate } = require("../models/User")
const e = require("express")

// api/loads/
router.post(
    "/",
    check("name", "Name not found").exists(),
    check("payload", "Must be an integer greater than 0").isInt({ gt: 0 }),
    check("pickup_address", "Pickup address not found").exists(),
    check("delivery_address", "Delivery address not found").exists(),
    check("dimensions", "Dimensions  not found").exists(),
    auth,
    isShipper,
    async (req, res) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: "Incorrect truck data",
                })
            }
            const user = await User.findById(req.user._id)
            if (!user) {
                return res.status(400).json({ message: "User not found" })
            } else if (user.role !== "SHIPPER") {
                return res
                    .status(400)
                    .json({ message: "User is not a shipper" })
            }

            const {
                name,
                payload,
                pickup_address,
                delivery_address,
                dimensions,
            } = req.body

            let logs = []
            let log = {
                message: `Load created.`,
                time: Date.now(),
            }
            logs.push(log)
            const load = new Load({
                created_by: user._id,
                name,
                payload,
                pickup_address,
                delivery_address,
                dimensions,
                logs,
            })

            sendEmail(user.email, log.message, "Load created successfully")

            await load.save()

            res.status(201).json({ message: "Load created successfully" })
        } catch (e) {
            res.status(500).json({ message: "Something went wrong, try again" })
        }
    }
)

router.get("/active", auth, isDriver, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        const load = await Load.findOne({
            assigned_to: user._id,
            status: "ASSIGNED",
        })
        if (!load) {
            return res.status(400).json({ message: "No active loads" })
        }

        res.status(200).json({ load })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

router.get("/:id/shipping_info", auth, isShipper, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }

        const { id } = req.params
        if (!id) {
            return res.status(400).json({ message: "Load not found" })
        }

        const load = await Load.findById(id)

        if (!load || load.created_by != user._id || load.assigned_to == "") {
            return res.status(400).json({ message: "Load not found" })
        }

        const truck = await Truck.findOne({ assigned_to: load.assigned_to })

        return res.status(200).json({ load, truck })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

router.get("/:id", auth, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        const { id } = req.params

        const load = await Load.findById(id)

        if (!load) {
            return res.status(400).json({ message: "Load not found" })
        }

        if (user.role === "DRIVER") {
            if (load.assigned_to == user._id) {
                return res.status(200).json({ load })
            }
        } else if (user.role === "SHIPPER") {
            if (load.created_by == user._id) {
                return res.status(200).json({ load })
            }
        }
        return res
            .status(400)
            .json({ message: "You don't have rules to see this load" })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

router.put("/:id", auth, isShipper, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }

        const { id } = req.params
        if (!id) {
            return res.status(400).json({ message: "Load not found" })
        }

        const load = await Load.findById(id)
        if (!load || load.status != "NEW" || load.created_by != user._id) {
            return res
                .status(400)
                .json({ message: "You don't have rules to see this load" })
        }

        // const {name, payload, pickup_address, delivery_address,dimensions} = req.body
        let logs = load.logs
        let log = {
            message: `Load id: ${load._id} details changed successfully`,
            time: Date.now(),
        }
        logs.push(log)
        let params = {
            name: req.body.name,
            payload: req.body.payload,
            pickup_address: req.body.pickup_address,
            delivery_address: req.body.delivery_address,
            dimensions: req.body.dimensions,
            logs,
        }

        for (let prop in params) if (!params[prop]) delete params[prop]

        await Load.findByIdAndUpdate(load.id, params)
        sendEmail(user.email, log.message, "Load deteils changed")

        return res
            .status(200)
            .json({ message: "Load details changed successfully" })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

router.delete("/:id", auth, isShipper, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }

        const { id } = req.params
        if (!id) {
            return res.status(400).json({ message: "Load not found" })
        }

        const load = await Load.findById(id)
        if (!load || load.status != "ASSIGNED" || load.created_by != user._id) {
            return res
                .status(400)
                .json({ message: "You don't have rules to see this load" })
        }
        await Load.findByIdAndRemove(id)
        sendEmail(user.email, "Load deleted", "Load deleted successfully")
        res.status(200).json({ message: "Load deleted successfully" })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

router.post("/:id/post", auth, isShipper, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }

        const { id } = req.params
        if (!id) {
            return res.status(400).json({ message: "Load not found" })
        }

        const load = await Load.findById(id)
        if (!load || load.created_by != user._id) {
            return res.status(400).json({ message: "Load not found" })
        }
        if (load.status != "NEW") {
            return res.status(400).json({ message: "Load already posted" })
        }
        await Load.findByIdAndUpdate(id, { status: "POSTED" })
        const arrWithTruckTypes = [
            "SPRINTER",
            "SMALL STRAIGHT",
            "LARGE STRAIGHT",
        ]
        let truckType = findTruck(load.dimensions, load.payload)
        if (!truckType) {
            await Load.findByIdAndUpdate(id, {
                status: "NEW",
            })
            return res.status(400).json({ message: "Dimensions is too big" })
        }
        const indexOfSuitableTruck = arrWithTruckTypes.indexOf(truckType)

        const arrWithSatisfactoryTruckTypes = arrWithTruckTypes.slice(
            indexOfSuitableTruck
        )
        const trucks = await Truck.find({
            type: arrWithSatisfactoryTruckTypes,
            assigned_to: { $ne: "" },
            status: "IS",
        })
        if (trucks.length > 0) {
            for (let i = 0; i < arrWithSatisfactoryTruckTypes.length; i++) {
                var firstAssutableTruck = trucks.find(
                    (element) =>
                        element.type === arrWithSatisfactoryTruckTypes[i]
                )
                if (firstAssutableTruck) break
            }

            const truck = await Truck.findOneAndUpdate(
                {
                    type: firstAssutableTruck.type,
                    assigned_to: { $ne: "" },
                    status: "IS",
                },
                { status: "OL" }
            )
            let logs = load.logs
            let log = {
                message: `Load posted successfully`,
                time: Date.now(),
            }
            logs.push(log)
            await Load.findByIdAndUpdate(id, {
                status: "ASSIGNED",
                state: "En route to Pick Up",
                assigned_to: truck.assigned_to,
                logs: logs,
            })
            sendEmail(user.email, log.message, "Load posted")

            return res.status(200).json({
                message: "Load posted successfully",
                driver_found: true,
            })
        } else {
            await Load.findByIdAndUpdate(id, {
                status: "NEW",
            })
            return res.status(400).json({
                message: "Load wasn't posted",
                driver_found: false,
            })
        }
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})
router.patch("/active/state", auth, isDriver, async (req, res) => {
    //TODO RDY
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        const arrWithStates = [
            "En route to Pick Up",
            "Arrived to Pick Up",
            "En route to delivery",
            "Arrived to delivery",
        ]
        const load = await Load.findOne({
            assigned_to: user._id,
            status: "ASSIGNED",
        })
        if (!load) {
            return res.status(400).json({ message: "No active loads" })
        }
        let logs = load.logs
        const loadCreator = await User.findById(load.created_by)

        const indexOfCurrStatus = arrWithStates.indexOf(load.state)
        let stateToShow = ""
        if (indexOfCurrStatus === arrWithStates.length - 2) {
            stateToShow = arrWithStates[indexOfCurrStatus + 1]
            let log = {
                message: `Load state changed to '${stateToShow}'`,
                time: Date.now(),
            }
            logs.push(log)
            await Load.findByIdAndUpdate(load._id, {
                state: arrWithStates[indexOfCurrStatus + 1],
                status: "SHIPPED",
                logs,
            })
            await Truck.findOneAndUpdate(
                { assigned_to: user._id },
                { assigned_to: "", status: "" }
            )
            sendEmail(user.email, log.message, "Load state changed")
            sendEmail(loadCreator.email, log.message, "Load state changed")
        } else {
            stateToShow = arrWithStates[indexOfCurrStatus + 1]

            let log = {
                message: `Load state changed to '${stateToShow}'`,
                time: Date.now(),
            }
            logs.push(log)
            await Load.findByIdAndUpdate(load._id, {
                state: arrWithStates[indexOfCurrStatus + 1],
                logs,
            })
            sendEmail(user.email, log.message, "Load state changed")
            sendEmail(loadCreator.email, log.message, "Load state changed")
        }
        res.status(200).json({
            message: `Load state changed to '${stateToShow}'`,
        })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})
router.get("/", auth, async (req, res) => {
    try {
        const { status, limit = 10, offset = 0 } = req.query

        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        if (user.role == "DRIVER") {
            const loads = await Load.find({
                assigned_to: user._id,
                status: status ? status : { $ne: "" },
            })
                .limit(parseInt(limit))
                .skip(parseInt(offset))
            res.status(200).json({ loads })
        } else if (user.role == "SHIPPER") {
            const loads = await Load.find({
                created_by: user._id,
                status: status ? status : { $ne: "" },
            })
                .limit(parseInt(limit))
                .skip(parseInt(offset))
            res.status(200).json({ loads })
        }
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

module.exports = router
