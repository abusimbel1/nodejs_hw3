const { Router } = require("express")
const fs = require("fs").promises
const router = Router()
const auth = require("../middleware/authMiddleware")
const bcrypt = require("bcryptjs")
const { uploadToCloudinary } = require("../additionalFunc/additionalFunc")
const rmdirRecursiveAsync = require("rmdir-recursive-async")
const driverFree = require("../middleware/driverFree")
const User = require("../models/User")
const Load = require("../models/Load")

// api/users/me
router.get("/me", auth, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        res.json({
            user: {
                _id: user._id,
                email: user.email,
                created_date: user.created_date,
            },
        })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

// api/users/me
router.delete("/me", auth, driverFree, async (req, res) => {
    try {
        const load = await Load.findOne({
            assigned_to: req.user._id,
            status: "ASSIGNED",
        })
        if (load) {
            return res.status(400).json({ message: "User is in load" })
        }
        const deletedUser = await User.findByIdAndDelete(req.user._id)
        if (!deletedUser) {
            return res.status(400).json({ message: "User not found" })
        }
        res.json({ message: "Profile deleted successfully" })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})
// api/users/me
router.patch("/me/password", auth, driverFree, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        const { oldPassword, newPassword } = req.body
        if (!oldPassword || !newPassword) {
            return res.status(400).json({ message: "No input params" })
        }
        const isMathPass = await bcrypt.compare(
            req.body.oldPassword,
            user.password
        )
        if (!isMathPass) {
            return res.status(400).json({ message: "Invalid old password" })
        }
        const hashedPass = await bcrypt.hash(newPassword, 10)
        await User.findByIdAndUpdate(req.user._id, { password: hashedPass })
        res.json({ message: "Password changed successfully" })
    } catch (e) {
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})
// api/users/me
router.patch("/me/avatar", auth, driverFree, async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        if (!user) {
            return res.status(400).json({ message: "User not found" })
        }
        const file = req.files ? req.files.image.tempFilePath : null
        let userImage = ""
        if (file) {
            const imgUrl = await uploadToCloudinary(file)
            userImage = imgUrl.url
            await rmdirRecursiveAsync("tmp")
        } else {
            return res.status(400).json({ message: "Avatar not found" })
        }
        await User.findByIdAndUpdate(req.user._id, { photo: userImage })
        res.json({ message: "Avatar changed successfully" })
    } catch (e) {
        console.log(e.message)
        res.status(500).json({ message: "Something went wrong, try again" })
    }
})

module.exports = router
