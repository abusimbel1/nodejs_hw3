const { Router } = require("express")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const nodemailer = require("nodemailer")
const { check, validationResult, checkSchema } = require("express-validator")
const { uploadToCloudinary } = require("../additionalFunc/additionalFunc")
const rmdirRecursiveAsync = require("rmdir-recursive-async")
const config = require("config")
const User = require("../models/User")
const router = Router()

var schema = {
    role: {
        in: "body",
        isIn: {
            options: [["SHIPPER", "DRIVER"]],
            errorMessage: "Invalid role",
        },
    },
}

// /api/auth/register
router.post(
    "/register",
    [
        check("email", "Incorrect email").isEmail(),
        check("password", "Minimum length 4 characters").isLength({ min: 4 }),
        checkSchema(schema),
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: "Incorrect registration data",
                })
            }

            const { email, password, role } = req.body

            const candidate = await User.findOne({ email })

            if (candidate) {
                return res
                    .status(400)
                    .json({ message: "This user already exist" })
            }

            const hashedPass = await bcrypt.hash(password, 10)
            const file = req.files ? req.files.image.tempFilePath : null
            let userImage = ""
            if (file) {
                const imgUrl = await uploadToCloudinary(file)
                userImage = imgUrl.url
                await rmdirRecursiveAsync("tmp")
            } else {
                userImage =
                    "https://res.cloudinary.com/dvq7tirs8/image/upload/v1605878487/qiayx1ipcuwps83bjyan.png"
            }

            const user = new User({
                email,
                password: hashedPass,
                role,
                photo: userImage,
            })

            await user.save()

            res.status(201).json({ message: "Profile created successfully" })
        } catch (e) {
            console.log(e.message)
            res.status(500).json({ message: "Something went wrong, try again" })
        }
    }
)

//./api/auth/login
router.post(
    "/login",
    [
        check("email", "Incorrect email").normalizeEmail().isEmail(),
        check("password", "Enter password").exists(),
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: "Incorrect login data",
                })
            }

            const { email, password } = req.body
            const user = await User.findOne({ email })

            if (!user) {
                return res
                    .status(400)
                    .json({ message: "Incorrect email or password" })
            }

            const isMathPass = await bcrypt.compare(password, user.password)

            if (!isMathPass) {
                return res
                    .status(400)
                    .json({ message: "Incorrect email or password" })
            }

            const token = jwt.sign(
                JSON.stringify(user),
                config.get("jwtSectet")
            )

            res.json({ jwt_token: token })
        } catch (e) {
            console.log(e.message)
            res.status(500).json({ message: "Something went wrong, try again" })
        }
    }
)
router.post(
    "/forgot_pasword",
    [check("email", "Incorrect email").normalizeEmail().isEmail()],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: "Incorrect login data",
                })
            }

            const { email } = req.body
            const user = await User.findOne({ email })

            if (!user) {
                return res.status(400).json({ message: "No user found with this email" })
            }

            var chars =
                "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            var string_length = 8
            var randomstring = ""
            for (var i = 0; i < string_length; i++) {
                var rnum = Math.floor(Math.random() * chars.length)
                randomstring += chars.substring(rnum, rnum + 1)
            }
            const hashedPass = await bcrypt.hash(randomstring, 10)

            let transporter = nodemailer.createTransport({
                service: "gmail",
                auth: {
                    user: "abusimbel24@gmail.com", // TODO: your gmail account
                    pass: "Ukraine2001", // TODO: your gmail password
                },
            })

            let mailOptions = {
                from: "abusimbel24@gmail.com",
                to: email,
                subject: "Password reset",
                text: `Your new password: ${randomstring}`,
                html: `Your new password: <b>${randomstring}<b>`,
            }
            await User.findByIdAndUpdate(user._id, { password: hashedPass })
            await transporter.sendMail(mailOptions)

            res.status(200).json({
                message: "New password sent to your email address",
            })
        } catch (e) {
            console.log(e.message)
            res.status(500).json({ message: "Something went wrong, try again" })
        }
    }
)

module.exports = router
