const express = require("express")
const config = require("config")
const mongoose = require("mongoose")
var cors = require("cors")
const fileUpload = require("express-fileupload")

const app = express()

app.use(express.json({ extended: true }))
app.use(cors())
app.use(
    fileUpload({
        useTempFiles: true,
    })
)

app.use("/api/auth", require("./routes/authRoutes"))
app.use("/api/users", require("./routes/userRoutes"))
app.use("/api/trucks", require("./routes/truckRoutes"))
app.use("/api/loads", require("./routes/loadRoutes"))

const PORT = config.get("serverPort") || 3333

if (process.env.NODE_ENV === "test") {
    start(config.get("testMongoUri"))
} else {
    start(config.get("mongoUri"))
}
async function start(url) {
    try {
        await mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,
        })
        app.listen(process.env.PORT || PORT, () =>
            console.log(`App has been started on port ${PORT}...`)
        )
    } catch (e) {
        console.log("Server Error", e.message)
        process.exit(1)
    }
}

module.exports = app
