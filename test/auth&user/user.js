const supertest = require("supertest")
const assert = require("assert")
const app = require("../../app")
const { expect } = require("chai")

describe.only("USER /api/users", function () {
    describe("Positive tests", () => {
        it("GET User profile info", async () => {
            const data = {
                email: "tester123@gmail.com",
                password: "admin",
                role: "DRIVER",
            }
            await supertest(app).post("/api/auth/register").send(data)
            const resLogin = await supertest(app)
                .post("/api/auth/login")
                .send(data)
            const resUserInfo = await supertest(app)
                .get("/api/users/me")
                .set("Authorization", `Bearer ${resLogin.body.jwt_token}`)
                .expect(200)

            expect(resUserInfo.body.user).to.not.be.empty
        })

        it("PATCH Change user's password", async () => {
            const data = {
                email: "tester123@gmail.com",
                password: "admin",
            }
            const res = await supertest(app).post("/api/auth/login").send(data)
            const newPasswordData = {
                oldPassword: data.password,
                newPassword: "qwerqwer",
            }
            const resUserInfo = await supertest(app)
                .patch("/api/users/me/password")
                .set("Authorization", `Bearer ${res.body.jwt_token}`)
                .send(newPasswordData)
                .expect(200)

            expect(resUserInfo.body.message).to.eq(
                "Password changed successfully"
            )
        })

        it("DELETE Delete user profile", async () => {
            const data = {
                email: "test123@gmail.com",
                password: "admin",
                role: "DRIVER",
            }
            await supertest(app).post("/api/auth/register").send(data)
            const res = await supertest(app).post("/api/auth/login").send(data)
            const resDeletedUser = await supertest(app)
                .delete("/api/users/me")
                .set("Authorization", `Bearer ${res.body.jwt_token}`)
                .expect(200)
            expect(resDeletedUser.body.message).to.eq(
                "Profile deleted successfully"
            )
        })
    })

    describe("Negative tests", () => {
        it("GET User profile info without jwt_token", async () => {
            await supertest(app).get("/api/users/me").expect(401)
        })

        it("PATCH Change user's password with incorrect old password", async () => {
            const data = {
                email: "tester123@gmail.com",
                password: "qwerqwer",
            }
            const res = await supertest(app).post("/api/auth/login").send(data)
            const newPasswordData = {
                oldPassword: "wrongPass",
                newPassword: "qwerqwer",
            }
            const resUserInfo = await supertest(app)
                .patch("/api/users/me/password")
                .set("Authorization", `Bearer ${res.body.jwt_token}`)
                .send(newPasswordData)
                .expect(400)
            expect(resUserInfo.body.message).to.eq("Invalid old password")
        })

        it("DELETE Delete user profile without jwt_token", async () => {
            await supertest(app).delete("/api/users/me").expect(401)
        })
    })
})
