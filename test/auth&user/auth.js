process.env.NODE_ENV = "test"

const supertest = require("supertest")
const assert = require("assert")
const app = require("../../app")
const { expect } = require("chai")
const User = require("../../models/User")
const Truck = require("../../models/Truck")
const Load = require("../../models/Load")

describe.only("AUTH /api/auth", function () {
    before(async () => {
        await User.deleteMany({})
        await Truck.deleteMany({})
        await Load.deleteMany({})
    })
    afterEach(async () => {
        // await User.deleteMany({})
    })
    after(async () => {
        await User.deleteMany({})
        await Truck.deleteMany({})
        await Load.deleteMany({})
    })

    describe("Positive tests", () => {
        it("POST Register user in system with all corect credentials", async () => {
            const data = {
                email: "test@gmail.com",
                password: "admin",
                role: "DRIVER",
            }
            const res = await supertest(app)
                .post("/api/auth/register")
                .send(data)

            expect(res.body.message).to.eq("Profile created successfully")
        })

        it("POST Login user in system with all corect credentials", async () => {
            const data = {
                email: "test@gmail.com",
                password: "admin",
            }
            const res = await supertest(app).post("/api/auth/login").send(data)

            expect(res.body.jwt_token).to.not.be.empty
        })

        it("POST Forgot password option with all corect credentials", async () => {
            const data = {
                email: "abusimbel2001@gmail.com",
                password: "admin",
                role: "DRIVER",
            }
            await supertest(app).post("/api/auth/register").send(data)

            const emailData = {
                email: "abusimbel2001@gmail.com",
            }
            const res = await supertest(app)
                .post("/api/auth/forgot_pasword")
                .send(emailData)
                .expect(200)

            expect(res.body.message).to.eq(
                "New password sent to your email address"
            )
        })
    })
    describe("Negative tests", () => {
        it("POST Login user in system with incorect credentials", async () => {
            const data = {
                email: "test@gmail.com",
            }
            const res = await supertest(app)
                .post("/api/auth/login")
                .send(data)
                .expect(400)
            expect(res.body.message).to.eq("Incorrect login data")
        })

        it("POST Register user in system with incorect credentials", async () => {
            const data = {
                email: "test@gmail.com",
                password: "qqqq",
            }
            const res = await supertest(app)
                .post("/api/auth/register")
                .send(data)
                .expect(400)
            expect(res.body.message).to.eq("Incorrect registration data")
        })

        it("POST Forgot password option incorect credentials", async () => {
            const emailData = {
                email: "testovich@gmail.com",
            }
            const res = await supertest(app)
                .post("/api/auth/forgot_pasword")
                .send(emailData)
                .expect(400)

            expect(res.body.message).to.eq("No user found with this email")
        })
    })
})
