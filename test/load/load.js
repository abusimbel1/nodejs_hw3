const supertest = require("supertest")
const assert = require("assert")
const app = require("../../app")
const { expect } = require("chai")
const User = require("../../models/User")
const Truck = require("../../models/Truck")
const Load = require("../../models/Load")

describe.only("LOADS /api/loads", function () {
    before(async () => {
        await User.deleteMany({})
        await Truck.deleteMany({})
        await Load.deleteMany({})
    })
    afterEach(async () => {})
    after(async () => {
        // await User.deleteMany({})
        // await Truck.deleteMany({})
        // await Load.deleteMany({})
    })
    describe("Positive tests", () => {
        let loadIdToPost = ""
        let truckIdToAssign = ""
        it("POST create load with all corect credentials", async () => {
            const data = {
                email: "testuser@gmail.com",
                password: "admin",
                role: "SHIPPER",
            }
            await supertest(app).post("/api/auth/register").send(data)
            const resLogin = await supertest(app)
                .post("/api/auth/login")
                .send(data)
            const loadData = {
                name: "Moving sofa",
                payload: 100,
                pickup_address:
                    "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
                delivery_address: "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
                dimensions: {
                    width: 44,
                    length: 32,
                    height: 66,
                },
            }
            const res = await supertest(app)
                .post("/api/loads")
                .send(loadData)
                .set("Authorization", `Bearer ${resLogin.body.jwt_token}`)
                .expect(201)
            expect(res.body.message).to.eq("Load created successfully")
        })
        it("GET get all loads", async () => {
            const data = {
                email: "testuser@gmail.com",
                password: "admin",
            }
            const resLogin = await supertest(app)
                .post("/api/auth/login")
                .send(data)
            const res = await supertest(app)
                .get("/api/loads")
                .set("Authorization", `Bearer ${resLogin.body.jwt_token}`)
                .expect(200)
            loadIdToPost = res.body.loads[0]._id
            expect(res.body.loads.length).to.eq(1)
        })
        it("POST post load by id", async () => {
            const data = {
                email: "testuser@gmail.com",
                password: "admin",
            }
            const resLogin = await supertest(app)
                .post("/api/auth/login")
                .send(data)
            const userData = {
                email: "testdriver@gmail.com",
                password: "admin",
                role: "DRIVER",
            }
            await supertest(app).post("/api/auth/register").send(userData)
            const resLoginDriver = await supertest(app)
                .post("/api/auth/login")
                .send(userData)
            const resTruckCreate = await supertest(app)
                .post("/api/trucks")
                .send({ type: "SPRINTER" })
                .set("Authorization", `Bearer ${resLoginDriver.body.jwt_token}`)
            const resTruckGet = await supertest(app)
                .get("/api/trucks")
                .set("Authorization", `Bearer ${resLoginDriver.body.jwt_token}`)
            truckIdToAssign = resTruckGet.body[0].trucks[0]._id
            const resTruckAssign = await supertest(app)
                .post(`/api/trucks/${truckIdToAssign}/assign`)
                .set("Authorization", `Bearer ${resLoginDriver.body.jwt_token}`)
            const res = await supertest(app)
                .post(`/api/loads/${loadIdToPost}/post`)
                .set("Authorization", `Bearer ${resLogin.body.jwt_token}`)
                .expect(200)
            expect(res.body.message).to.eq("Load posted successfully")
        })
    })
    describe("Negative tests", () => {
        it("GET get load without authorization", async () => {
            const res = await supertest(app).get("/api/loads").expect(401)
        })
        it("POST create load incorect credentials", async () => {
            const data = {
                email: "testuser@gmail.com",
                password: "admin",
            }
            const resLogin = await supertest(app)
                .post("/api/auth/login")
                .send(data)
            const loadData = {
                name: "Moving sofa",
                payload: 100,
                pickup_address:
                    "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
                delivery_address: "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
            }
            const res = await supertest(app)
                .post("/api/loads")
                .send(loadData)
                .set("Authorization", `Bearer ${resLogin.body.jwt_token}`)
                .expect(400)
            expect(res.body.message).to.eq("Incorrect truck data")
        })
    })
})
