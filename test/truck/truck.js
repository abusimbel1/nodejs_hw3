const supertest = require("supertest")
const assert = require("assert")
const app = require("../../app")
const { expect } = require("chai")
const User = require("../../models/User")
const Truck = require("../../models/Truck")
const Load = require("../../models/Load")

describe.only("TRUCKS /api/trucks", function () {
    var truckIdToDelete = ""
    before(async () => {
        await User.deleteMany({})
        await Truck.deleteMany({})
        await Load.deleteMany({})
    })
    afterEach(async () => {})
    after(async () => {
        // await User.deleteMany({})
        // await Truck.deleteMany({})
        // await Load.deleteMany({})
    })
    describe("Positive tests", () => {
        it("POST create truck with all corect credentials", async () => {
            const data = {
                email: "tester@gmail.com",
                password: "admin",
                role: "DRIVER",
            }
            await supertest(app).post("/api/auth/register").send(data)
            const resLogin = await supertest(app)
                .post("/api/auth/login")
                .send(data)
            const truckData = {
                type: "SPRINTER",
            }
            const res = await supertest(app)
                .post("/api/trucks")
                .send(truckData)
                .set("Authorization", `Bearer ${resLogin.body.jwt_token}`)
                .expect(201)
            expect(res.body.message).to.eq("Truck created successfully")
        })

        it("GET Get all driver trucks", async () => {
            const data = {
                email: "tester@gmail.com",
                password: "admin",
            }
            const resLogin = await supertest(app)
                .post("/api/auth/login")
                .send(data)

            const res = await supertest(app)
                .get("/api/trucks")
                .set("Authorization", `Bearer ${resLogin.body.jwt_token}`)
                .expect(200)
            truckIdToDelete = res.body[0].trucks[0]._id
            expect(res.body.length).to.eq(1)
        })

        it("DELETE Delete created truck by id", async () => {
            const data = {
                email: "tester@gmail.com",
                password: "admin",
            }
            const resLogin = await supertest(app)
                .post(`/api/auth/login`)
                .send(data)

            const res = await supertest(app)
                .delete(`/api/trucks/${truckIdToDelete}`)
                .set("Authorization", `Bearer ${resLogin.body.jwt_token}`)
                .expect(200)
            expect(res.body.message).to.eq("Truck deleted successfully")
        })
    })

    describe("Negative tests", () => {
        it("POST create truck with incorect credentials", async () => {
            const data = {
                email: "tester@gmail.com",
                password: "admin",
            }
            const resLogin = await supertest(app)
                .post("/api/auth/login")
                .send(data)
            const truckData = {
                type: "none",
            }
            const res = await supertest(app)
                .post("/api/trucks")
                .send(truckData)
                .set("Authorization", `Bearer ${resLogin.body.jwt_token}`)
                .expect(400)
        })
        it("DELETE Delete created truck by incorrect id", async () => {
            const data = {
                email: "tester@gmail.com",
                password: "admin",
            }
            // await supertest(app).post("/api/auth/register").send(data)
            const resLogin = await supertest(app)
                .post("/api/auth/login")
                .send(data)
            const res = await supertest(app)
                .delete(`/api/trucks/5fbace73bdd7fa29777117f6`)
                .set("Authorization", `Bearer ${resLogin.body.jwt_token}`)
                .expect(400)
        })
    })
})
