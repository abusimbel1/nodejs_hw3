const e = require("express")
const cloudinary = require("cloudinary").v2
const nodemailer = require("nodemailer")
const config = require("config")
module.exports.cloudinary = cloudinary.config({
    cloud_name: config.get("cloud_name"),
    api_key: config.get("api_key"),
    api_secret: config.get("api_secret"),
})

module.exports.findTruck = ({ width, length, height }, maxWeight) => {
    if (width <= 300 && length <= 250 && height <= 170 && maxWeight <= 1700) {
        return "SPRINTER"
    } else if (
        width <= 500 &&
        length <= 250 &&
        height <= 170 &&
        maxWeight <= 2500
    ) {
        return "SMALL STRAIGHT"
    } else if (
        width <= 700 &&
        length <= 350 &&
        height <= 200 &&
        maxWeight <= 4000
    ) {
        return "LARGE STRAIGHT"
    } else {
        return null
    }
}
module.exports.sendEmail = async (to,text, subject) => {
    let transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: "abusimbel24@gmail.com", // TODO: your gmail account
            pass: "Ukraine2001", // TODO: your gmail password
        },
    })
    let mailOptions = {
        from: "abusimbel24@gmail.com",
        to,
        subject,
        text: text,
    }

    await transporter.sendMail(mailOptions)
}

module.exports.uploadToCloudinary = (image) => {
    return new Promise((resolve, reject) => {
        cloudinary.uploader.upload(image, (err, url) => {
            if (err) return reject(err)
            return resolve(url)
        })
    })
}
