# Nodejs_hw3

 This is like UBER backend app in REST style, using MongoDB as database. This service should help regular people to deliver their stuff and help drivers to find loads and earn some money. Application contains 2 roles, driver and shipper.

## Such additional tasks were done by me

- Any system user can easily reset his password using 'forgot password' option;
- User is able to attach a photo to his profile(When creating a user, if the picture was not attached, it is assigned a default, but there is an api request patch "/me/avatar" and you must to attach the desired file);
- Ability to filter loads by status;
- Any system user can get notifications through the email about shipment updates;
- Pagination for loads;
- The most important functionality covered with unit and acceptance tests;

## Installation
To install a project, you must first clone it:

```bash
git clone https://gitlab.com/abusimbel1/nodejs_hw3.git	
```

Use the package manager [npm](https://nodejs.org/en/) to install node_modules folder.

```bash
npm install
```

## Available Scripts

```bash
npm start
npm server
```
## Tests

```bash
npm test
```
## Contributor

Oleksandr Hrytsiuk

## License
[MIT](https://choosealicense.com/licenses/mit/)